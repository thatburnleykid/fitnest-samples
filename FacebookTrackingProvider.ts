import { AppEventsLogger as RNFBAppEventsLogger } from 'react-native-fbsdk';
import _cloneDeep from 'lodash/cloneDeep';

import isProduction from '../modules/isProduction';
import { TrackingServiceInterface, TrackingServiceSetUserProperty } from '../types/TrackingServiceInterface';
import { User } from '../types/User';
import SYSubscription from '../modules/SYSubscription';
import { TRIAL_CURRENCY, TRIAL_ORDER_ID } from '../constants';
import {
    ADD_TO_CART_EVENT,
    COMPLETED_REGISTRATION_EVENT,
    START_TRIAL_EVENT,
    SUBSCRIPTION_PURCHASE_INITIATED_EVENT,
    SUBSCRIPTION_PURCHASE_SUCCESS_EVENT,
    FIRST_TRAINING_STARTED_EVENT,
    FIRST_TRAINING_COMPLETED_EVENT,
} from '../redux/store/trackingEventNames';
import {
    SubscriptionPurchaseSuccessPayload,
    FirstTrainingStartedAction,
    SubscriptionPurchaseInitiatedPayload,
} from '../types/Action';

type Params = {
    [key: string]: string | number;
};

const logEvent = (eventName: string, ...args: Array<any>) => {
    // if (!isProduction()) {
    //   console.group('FacebookTrackingService logEvent');
    //   console.log(`eventName: "${eventName}"`);
    //   console.log('eventParams', ...args);
    //   console.groupEnd();
    //   return undefined;
    // }

    return RNFBAppEventsLogger.logEvent(eventName, ...args);
};

const logPurchase = (purchaseAmount: number, currencyCode: string, parameters?: Params | null | undefined) => {
    // if (!isProduction()) {
    //   console.log('FacebookTrackingService logPurchase', purchaseAmount, currencyCode, parameters);
    //   return undefined;
    // }

    return RNFBAppEventsLogger.logPurchase(purchaseAmount, currencyCode, parameters || undefined);
};

/**
 * For parameter see
 * https://developers.facebook.com/docs/app-events/getting-started-app-events-ios
 */
type TrackingServiceInterfaceFunction = (a?: any) => void
class FacebookTrackingProvider implements TrackingServiceInterface {
    specialFunctions: Map<string, TrackingServiceInterfaceFunction> = new Map<string, TrackingServiceInterfaceFunction>([
        [ADD_TO_CART_EVENT, this.trackAddToCart],
        [SUBSCRIPTION_PURCHASE_SUCCESS_EVENT, this.trackSubscriptionSuccess],
        [START_TRIAL_EVENT, this.trackStartTrial],
        [COMPLETED_REGISTRATION_EVENT, this.trackCompleteRegistration],
        [SUBSCRIPTION_PURCHASE_INITIATED_EVENT, this.trackCheckoutInitiated],
        [FIRST_TRAINING_STARTED_EVENT, this.trackFirstTrainingStarted],
        [FIRST_TRAINING_COMPLETED_EVENT, this.trackFirstTrainingCompleted],
    ]);

    async init() { //
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    setUserContext(user: User) { /* do nothing */ }

    clearUserContext() { /* do nothing */ }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    setUserProperty(keyValuePair: TrackingServiceSetUserProperty) { /* do nothing */ }

    async trackView(screenName: string, screenParams?: {
        [key: string]: any;
    }): Promise<void> {
        return this.trackEvent(`View_${screenName}`, screenParams);
    }

    async trackEvent(passedEventName: string, eventParams?: {
        [key: string]: any;
    }): Promise<void> {
        const clonedParameter = _cloneDeep(eventParams);
        const trackingFunction = this.specialFunctions.get(passedEventName);

        if (trackingFunction) {
            trackingFunction(clonedParameter);
        } else {
            logEvent(passedEventName, clonedParameter);
        }
    }

    async trackFirstTrainingStarted(eventParams: FirstTrainingStartedAction['payload']): Promise<void> {
        logEvent('fb_mobile_tutorial_completion', {
            fb_success: 1,
            fb_content_id: eventParams.workoutId.toString(),
        });
    }

    async trackFirstTrainingCompleted(): Promise<void> {
        logEvent('fb_mobile_achievement_unlocked', {
            fb_description: 'first training completed',
        });
    }

    async trackAddToCart(subscription: SYSubscription): Promise<void> {
        const eventName = 'fb_mobile_add_to_cart';
        const params = {
            fb_content_type: subscription.title,
            fb_content_id: subscription.id,
            fb_currency: subscription.currency,
        };
        logEvent(eventName, params);
    }

    async trackSubscriptionSuccess(payload: SubscriptionPurchaseSuccessPayload): Promise<void> {
        const {
            subscription,
        } = payload;
        if (!subscription) {
            return;
        }

        logPurchase(subscription.price || 38.99, subscription.currency || 'USD');
    }

    async trackStartTrial() {
        logEvent('StartTrial', 0, {
            fb_order_id: TRIAL_ORDER_ID,
            fb_currency: TRIAL_CURRENCY,
        });
    }

    async trackCompleteRegistration() {
        logEvent('fb_mobile_complete_registration', {
            fb_registration_method: 'Email',
        });
    }

    async trackCheckoutInitiated(subscription: SubscriptionPurchaseInitiatedPayload): Promise<void> {
        const eventName = 'fb_mobile_initiated_checkout';
        const params = {
            fb_content_type: subscription.productName,
            fb_content_id: subscription.productId,
            fb_num_items: 1,
            fb_payment_info_available: 0,
            fb_currency: subscription.currency,
        };
        logEvent(eventName, params);
    }
}

export default FacebookTrackingProvider;
