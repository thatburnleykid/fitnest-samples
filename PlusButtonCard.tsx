import React from "react";
import { GestureResponderEvent, Image, ImageSourcePropType, StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native";
import { components } from "..";
import { theme } from "../../theme";
import { utils } from "../../utils";

interface PlusButtonCardProp {
    icon?: ImageSourcePropType,
    title: string,
    desc?: string,
    onPress?: ((event: GestureResponderEvent) => void) | undefined
    containerStyle?: StyleProp<ViewStyle>,
    titleStyle: StyleProp<TextStyle>,
    rightIcon?: ImageSourcePropType
}

export const PlusButtonCard: React.FC<PlusButtonCardProp> = ({ icon, title, desc, containerStyle, titleStyle, onPress, rightIcon }) => (
    <View style={[{
        backgroundColor: theme.color.COLOR_WHITE,
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingVertical: utils.ui.heightInPixel(20),
        paddingHorizontal: utils.ui.widthInPixel(15),
        marginBottom: utils.ui.heightInPixel(15),
        borderRadius: utils.ui.widthInPixel(5),

    }, containerStyle]}>
        <View style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
        }}>
            {icon && <Image source={icon} />}
            <View style={icon ? { marginLeft: utils.ui.widthInPixel(11) } : {}}>
                <components.texts.apercu.TextWeight500 fontSize={theme.fontSize.FONT_15} style={titleStyle}>
                    {title}
                </components.texts.apercu.TextWeight500>
                {desc && <components.texts.apercu.TextWeight500 style={{ opacity: .5 }}>
                    {desc}
                </components.texts.apercu.TextWeight500>}
            </View>
        </View>
        <TouchableOpacity activeOpacity={.7} onPress={onPress}>
            <Image source={rightIcon || require('../../assets/icons/plus.png')} />
        </TouchableOpacity>
    </View>
)