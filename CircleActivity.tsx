import * as React from "react";
import _get from "lodash/get";
import _findIndex from "lodash/findIndex";
import { StyleSheet, View } from "react-native";
import Container from "../../../components/Container";
import type { Props } from "../../../types/Props";
import AbstractView from "../../AbstractView";
import type { ViewInterface } from "../../AbstractView";
import { _R } from "../../../R";
import { CIRCLE_ACTIVITY_VIEW_NAME, PRIMARY_COLOR } from "../../../constants";
import { HorizontalView } from "../../../components/HorizontalView";
import SYPrimaryButton from "../../../components/buttons/SYPrimaryButton";
import { ScrollView } from "react-native-gesture-handler";
import SYTabs from "../../../components/SYTabs";

class CircleActivity
    extends AbstractView<
    Props & { userId?: string },
    {
    }
    >
    implements ViewInterface {
    // For tracking
    readonly viewName: string = CIRCLE_ACTIVITY_VIEW_NAME;

    state = {
    };

    updateState = (key: string, value: boolean) => {
        this.setState({
            ...this.state,
            [key]: value,
        });
    };


    componentDidMount() {
        super.componentDidMount();
    }

    componentDidAppear() {
        super.componentDidAppear();
    }


    render() {
        const {
            componentId,
            settings: { bottomTabsHeight },
        } = this.props;

        const containerCss = {
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 0,
            paddingBottom: bottomTabsHeight,
        };

        return (
            <Container
                componentId={componentId}
                style={containerCss}
                wrapSafeAreaView={false}
                topBar
                showTopBarBackButton
                scroll={false}
            >
                <ScrollView style={{ height: _R.utils.ui.screenHeight }}>
                    <HorizontalView justifyContent="space-between" alignItems="center">
                        <_R.components.texts.oswald.TextWeight700
                            fontSize={_R.theme.fontSize.FONT_20}
                            style={this.styles.header}
                        >
                            ACTIVITY
                        </_R.components.texts.oswald.TextWeight700>

                    </HorizontalView>

                    <SYTabs
                        tabStyle={{
                            container: this.styles.tabContainer,
                            text: this.styles.tabText
                        }}
                        activeTabStyle={{
                            container: this.styles.activeTabContainer,
                            text: this.styles.activeTabText
                        }}
                        tabItems={[
                            {
                                title: 'INFO',
                                content: (
                                    <>
                                        <HorizontalView alignItems="center">
                                            <_R.components.Avatar
                                                uri="https://i.pravatar.cc/160"
                                                imageWidth={84}
                                                borderColor={_R.theme.color.COLOR_WHITE}
                                                borderWidth={3}
                                                shadow
                                                shadowLevel={10}
                                            />

                                            <View style={{ marginLeft: 15 }}>
                                                <HorizontalView alignItems="baseline">
                                                    <_R.components.texts.apercu.TextWeight500 fontSize={_R.theme.fontSize.FONT_20}>
                                                        John Martin
                                                    </_R.components.texts.apercu.TextWeight500>
                                                </HorizontalView>
                                                <_R.components.texts.apercu.TextWeight500 color="#848491" style={{ marginTop: _R.utils.ui.heightInPixel(5) }}>
                                                    Age: 36 Years Week: 0  Day: 2
                                                </_R.components.texts.apercu.TextWeight500>
                                            </View>
                                        </HorizontalView>

                                        <HorizontalView mt={30} mb={30} justifyContent="space-between">
                                            <SYPrimaryButton title="Call" style={this.styles.w164} titleStyle={{ letterSpacing: 1 }} />
                                            <SYPrimaryButton title="Message" style={this.styles.w164} type="outline" titleStyle={{ letterSpacing: 1 }} />
                                        </HorizontalView>

                                        <_R.components.texts.apercu.TextWeight500 fontSize={_R.theme.fontSize.FONT_16}>
                                            MEASUREMENTS
                                        </_R.components.texts.apercu.TextWeight500>
                                        <HorizontalView mt={20} justifyContent="space-around" >
                                            <View >
                                                <_R.components.texts.apercu.TextWeight500 fontSize={_R.theme.fontSize.FONT_16}>
                                                    85
                                                </_R.components.texts.apercu.TextWeight500>
                                                <_R.components.texts.apercu.TextWeight300 fontSize={_R.theme.fontSize.FONT_12} style={{ marginTop: 8 }}>
                                                    WEIGHT
                                                </_R.components.texts.apercu.TextWeight300>
                                            </View>
                                            <View>
                                                <_R.components.texts.apercu.TextWeight500 fontSize={_R.theme.fontSize.FONT_16}>
                                                    5Ft 2inch
                                                </_R.components.texts.apercu.TextWeight500>
                                                <_R.components.texts.apercu.TextWeight300 fontSize={_R.theme.fontSize.FONT_12} style={{ marginTop: 8 }}>
                                                    HEIGHT
                                                </_R.components.texts.apercu.TextWeight300>
                                            </View>
                                            <View>
                                                <_R.components.texts.apercu.TextWeight500 fontSize={_R.theme.fontSize.FONT_16}>
                                                    Beginner
                                                </_R.components.texts.apercu.TextWeight500>
                                                <_R.components.texts.apercu.TextWeight300 fontSize={_R.theme.fontSize.FONT_12} style={{ marginTop: 8 }}>
                                                    LEVEL
                                                </_R.components.texts.apercu.TextWeight300>
                                            </View>
                                        </HorizontalView>

                                    </>
                                ),
                            },
                            {
                                title: 'PLAN',
                                content: (
                                    <>
                                        <_R.components.texts.apercu.TextWeight300>04 Jan, 2021- 16 Feb, 2021</_R.components.texts.apercu.TextWeight300>
                                        <HorizontalView mb={20} mt={10}>
                                            <_R.components.cards.PlusButtonCard
                                                rightIcon={require('../../../assets/icons/edit.png')}
                                                title="Activity"
                                                desc="Meal plan, exercise"

                                            />
                                        </HorizontalView>
                                        <_R.components.texts.apercu.TextWeight300>30 Jan, 2021 - 26 Feb, 2021</_R.components.texts.apercu.TextWeight300>
                                        <HorizontalView mb={20} mt={10}>
                                            <_R.components.cards.PlusButtonCard
                                                rightIcon={require('../../../assets/icons/edit.png')}
                                                title="Meal Plan"
                                                desc="Meal plan, exercise"

                                            />
                                        </HorizontalView>
                                    </>
                                )
                            },
                        ]}
                    />




                </ScrollView>

            </Container>
        );
    }


    styles = StyleSheet.create({
        header: { marginVertical: _R.utils.ui.heightInPixel(20), letterSpacing: _R.utils.ui.widthInPixel(4) },
        w164: { width: _R.utils.ui.widthInPixel(164) },
        tabContainer: {
            marginHorizontal: _R.utils.ui.widthInPixel(20),
            borderBottomWidth: 0,
            marginBottom: _R.utils.ui.heightInPixel(10),
            paddingBottom: _R.utils.ui.heightInPixel(16)
        },
        tabText: {
            color: _R.theme.color.COLOR_PRIMARY_TEXT,
            fontWeight: "600",
            letterSpacing: 4
        },
        activeTabContainer: {
            borderBottomColor: PRIMARY_COLOR,
            borderBottomWidth: 0
        },
        activeTabText: {
            color: PRIMARY_COLOR,
        }


    });
}

export default CircleActivity;
